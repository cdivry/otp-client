#!/usr/bin/env python3

import json
import os
import pyotp
import sys
import getpass

SEEDS_FILE = os.path.expanduser("~") + "/.otp_seeds"

WARNING_ICON = "\033[1;31m" "/" "\033[1;0m" "!" "\033[1;31m" "\\" "\033[0m"

##########################################
# CRYPTOGRAPHY                           #
##########################################

def cypher(text : str, key : str):
    encoded = []
    for i in range(0, len(text)):
        encoded.append((ord(text[i]) + ord(key[i % len(key)])) % 127)
    return bytes(encoded)

def uncypher(content : bytes, key : str):
    decoded = []
    for i in range(0, len(content)):
        decoded.append(chr((content[i] - ord(key[i % len(key)])) % 127))
    data = check_uncyphered_json("".join(decoded))
    return data

def check_uncyphered_json(data : str):
    try:
        _ = json.loads(data)
    except json.decoder.JSONDecodeError:
        print("Seeds not properly uncyphered.")
        print("It may be due to wrong passphrase, or corrupted seeds file.")
        exit(0)
    return data

##########################################
# OTP                                    #
##########################################

def pin(secret : str):
    totp = pyotp.TOTP(secret)
    return totp.now()

##########################################
# FILES                                  #
##########################################

def write_seeds_file(content : bytes, filename : str = SEEDS_FILE):
    with open(filename, "wb") as fd:
        key = getpass.getpass("Passphrase for creating a new seeds file: ")
        fd.write(cypher(content, key))
        print(f"{os.path.abspath(filename)} file created.")

def check_seeds_file(filename : str = SEEDS_FILE):
    if not os.path.isfile(filename):
        write_seeds_file("{}", filename)
    chmod = str(oct(os.stat(filename).st_mode)[-3:])
    if chmod != "600":
        os.chmod(filename, 0o600)
        print(f"WARNING: chmod {chmod} of {filename} is too permissive, changed to 600.")

def read_seeds_file(filename : str = SEEDS_FILE):
    # print(f"file: {filename}")
    check_seeds_file(filename)
    with open(filename, "rb") as fd:
        buf = fd.read()
        key = getpass.getpass("Passphrase for reading seeds: ")
        decoded = uncypher(buf, key)
        # print(decoded)
    seeds = json.loads(decoded)
    return seeds

##########################################
# SEEDS                                  #
##########################################

def add_seed(name : str = None, value : str = None, filename : str = SEEDS_FILE):
    seeds = read_seeds_file(filename)
    if name is None:
        name = input("Seed name: ")
    if name in seeds:
        print(WARNING_ICON, f"{name} seed already exists with that name.")
        confirm = input(f"Do you want to override {name} seed ? (y/N)")
        if confirm.lower() != "y":
            sys.exit(0)
    if value is None:
        value = input("Seed value: ")
    seeds[name] = value
    data = json.dumps(seeds)
    write_seeds_file(data, )

def del_seed(name : str = None, filename : str = SEEDS_FILE):
    seeds = read_seeds_file(filename)
    if name is None:
        name = input("Seed name: ")
    if name in seeds:
        seeds.pop(name)
        data = json.dumps(seeds)
        write_seeds_file(data, )
    else:
        unknown_seed(seeds, name)

def list_seeds(seeds : dict = None, values : bool = False):
    if seeds is None:
        seeds = read_seeds_file()
    print(f"\nSEEDS: {len(seeds)}")
    if values == True:
        print("- " + "\n- ".join([k + ": " + v for k,v in seeds.items()]) + "\n")
    else:
        print("- " + "\n- ".join([k for k,v in seeds.items()]) + "\n")

def use_seed(name : str, seeds : dict = None):
    if seeds is None:
        seeds = read_seeds_file()
    if name not in seeds:
        unknown_seed(seeds, name)
    otp = pin(seeds[name].replace(" ", ""))
    print(f"OTP: {otp} ({name})")

def unknown_seed(seeds : dict, seedname : str):
    list_seeds(seeds)
    print(f"Unknown seed: {seedname}")
    print(f"Please add it to your seed file :")
    print(" ", sys.argv[0], "-a", "<seedname>")
    sys.exit(0)

##########################################
# USAGE                                  #
##########################################

def usage(argv : list = None):
    if argv is None:
        argv = sys.argv
    print("(use seed) %s <seedname>" % argv[0])
    print("(add seed) %s -a <seedname>" % argv[0])
    print("(add seed) %s -a <seedname> -v <seedvalue>" % argv[0])
    print("(del seed) %s -d <seedname>" % argv[0])
    print("(show all seeds) %s -s" % argv[0])

##########################################
#                                        #
# ENTRYPOINT                             #
#                                        #
# ARGUMENTS PARSING                      #
#                                        #
##########################################

if __name__ == '__main__':

    argv = sys.argv
    # not enough args
    if len(argv) < 2 or argv[1] == "":
        usage(argv)
        sys.exit(-1)

    # add
    elif argv[1] == "-a":
        if len(argv) == 3:
            add_seed(argv[2])
        elif len(argv) == 5 and argv[3] == "-v":
            add_seed(argv[2], argv[4])
        else:
            usage(argv)
            sys.exit(-1)

    # delete
    elif argv[1] == "-d":
        if len(argv) == 3:
            del_seed(argv[2])
        else:
            usage(argv)
            sys.exit(-1)

    # list
    elif argv[1] == "-l":
        if len(argv) == 2:
            list_seeds()
        else:
            usage(argv)
            sys.exit(-1)

    # show seeds
    elif argv[1] == "-s":
        if len(argv) == 2 and argv[1] == "-s":
            print(WARNING_ICON, "This will DISPLAY ALL SEEDS on the screen.")
            list_seeds(values=True)
        else:
            usage(argv)
            sys.exit(-1)

    # use
    elif len(argv) == 2 and argv[1][0] != "-":
        use_seed(argv[1])

    else:
        usage(argv)
        sys.exit(-1)

