# OTP Cold Client

A simple python OTP client, with seeds cyphering.

## Installation :

- Requierements :
This program needs pyotp library, you can install it into your favorite python environment via :
```bash
pip install pyotp
```


- Then, you can copy otp.py into a bin directory (used in PATH) :
```bash
cp otp.py /usr/bin/otp
```

- Finally, set execute permissions for this binary :
```bash
chmod +x /usr/bin/otp
```

This programm will create an `.otp_seeds` file into you home (if not exists) at each run.

## Usage :

```text
(use seed) ./otp.py <seedname>
(add seed) ./otp.py -a <seedname>
(add seed) ./otp.py -a <seedname> -v <seedvalue>
(del seed) ./otp.py -d <seedname>
(show all seeds) ./otp.py -s
```

## Examples :

Here is some passphrase protected options :

- list seeds :
```bash
otp -l
```

- add seed :
```bash
otp -a seed_name # prompt will ask for value
otp -a seed_name "00112233445566778899" # non-interactive
```

- delete seed :
```bash
otp -d seed_name # will check key existence
```

- show all seeds :
```bash
otp -s
```
